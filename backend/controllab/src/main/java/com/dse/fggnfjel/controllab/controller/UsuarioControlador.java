package com.dse.fggnfjel.controllab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dse.fggnfjel.controllab.domain.Usuario;
import com.dse.fggnfjel.controllab.model.UsuarioApi;
import com.dse.fggnfjel.controllab.servicio.UsuarioServicio;

@RestController
@RequestMapping("/v1/usuario")
public class UsuarioControlador {
	
	
	@Autowired 
	UsuarioServicio usuarioServicio;
	
	
	@GetMapping("")
	public List<Usuario> getUsuarios(){
		
		return this.usuarioServicio.getUsuarios();
		
	}
	
	/*
	@GetMapping("")
	public Usuario getUsuario(Long id){
	
		return this.usuarioServicio.getUsuario();
		
	}*/
	
	@PostMapping("")
	public Usuario setUsuario(UsuarioApi usuario) throws Exception {
		return this.usuarioServicio.saveOrUpdate(usuario);
	}
	
	

}
