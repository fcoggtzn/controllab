package com.dse.fggnfjel.controllab.servicio;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dse.fggnfjel.controllab.domain.TipoUsuario;
import com.dse.fggnfjel.controllab.domain.Usuario;
import com.dse.fggnfjel.controllab.model.UsuarioApi;
import com.dse.fggnfjel.controllab.repositorio.TipoUsuarioInterface;
import com.dse.fggnfjel.controllab.repositorio.UsuarioInterface;

@Service
public class UsuarioServicio {
	
	@Autowired
	UsuarioInterface usarioRepo;
	
	@Autowired
	TipoUsuarioInterface tipoUsuarioRepo;
	
	public List<Usuario> getUsuarios(){		
		return this.usarioRepo.findAll();		
	}
	
	public Usuario saveOrUpdate(UsuarioApi usuarioapi) throws Exception {
		
		Usuario usuario = new Usuario(); 
		Optional<TipoUsuario>  tu ;
		tu = this.tipoUsuarioRepo.findById(usuarioapi.getIdTipo());
		
		if (!tu.isPresent())
			throw new Exception("Tipo no existe ");
		usuario.setClave(usuarioapi.getPassword());
		usuario.setUsuario(usuarioapi.getUsuario());
		usuario.setIdsuario(usuarioapi.getIdUsuario());
		usuario.setTipoUsuaro(tu.get());
		
		return this.usarioRepo.save(usuario);
	}
	
	public Usuario getUsuario(String usuario) {
		return this.usarioRepo.findByUsuario(usuario);		
	}
	
	

}
