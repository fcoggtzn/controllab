package com.dse.fggnfjel.controllab.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;




@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idsuario ;
	
	private String usuario;
	
	private String clave;
	private String notas;
	
	private Date created_date = new Date(System.currentTimeMillis());
	
	@JoinColumn(name = "tipo_usuario_idtipo_usuario", referencedColumnName = "idTipoUsuario")
    @ManyToOne(optional = false)
	private TipoUsuario tipoUsuaro;
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
/*	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}*/
	
	public Long getIdsuario() {
		return idsuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public void setIdsuario(Long idsuario) {
		this.idsuario = idsuario;
	}
	public TipoUsuario getTipoUsuaro() {
		return tipoUsuaro;
	}
	public void setTipoUsuaro(TipoUsuario tipoUsuaro) {
		this.tipoUsuaro = tipoUsuaro;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	
	
	

}
