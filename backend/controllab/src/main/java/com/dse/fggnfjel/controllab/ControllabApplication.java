package com.dse.fggnfjel.controllab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControllabApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllabApplication.class, args);
	}

}
