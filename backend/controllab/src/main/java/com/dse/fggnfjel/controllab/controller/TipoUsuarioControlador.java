package com.dse.fggnfjel.controllab.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dse.fggnfjel.controllab.domain.TipoUsuario;
import com.dse.fggnfjel.controllab.servicio.TipoUsuarioServicio;

@RestController
@RequestMapping("/v1/tipo_usuario")
public class TipoUsuarioControlador {
	
	
	@Autowired 
	TipoUsuarioServicio tipoUsuarioServicio;
	
	
	@GetMapping("")
	public List<TipoUsuario> getUsuarios(){
		
		return this.tipoUsuarioServicio.getTipoUsuarios();
		
	}
	
	
	@PostMapping("")
	public void setTipo(TipoUsuario tusuario) {
		this.tipoUsuarioServicio.saveOrUpdate(tusuario);
	}
	
	

}
