package com.dse.fggnfjel.controllab.repositorio;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dse.fggnfjel.controllab.domain.TipoUsuario;


public interface TipoUsuarioInterface  extends CrudRepository<TipoUsuario,Long> {
	@Override
    List<TipoUsuario> findAll();
	
	
}
