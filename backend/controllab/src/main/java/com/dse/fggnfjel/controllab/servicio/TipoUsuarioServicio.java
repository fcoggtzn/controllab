package com.dse.fggnfjel.controllab.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dse.fggnfjel.controllab.domain.TipoUsuario;
import com.dse.fggnfjel.controllab.repositorio.TipoUsuarioInterface;

@Service
public class TipoUsuarioServicio {
	
	@Autowired
	TipoUsuarioInterface tusarioRepo;
	
	public List<TipoUsuario> getTipoUsuarios(){		
		return this.tusarioRepo.findAll();		
	}
	
	public void saveOrUpdate(TipoUsuario tipoUsuario) {
		this.tusarioRepo.save(tipoUsuario);
	}
	
	

}
