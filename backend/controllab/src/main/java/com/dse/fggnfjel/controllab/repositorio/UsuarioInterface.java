package com.dse.fggnfjel.controllab.repositorio;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.dse.fggnfjel.controllab.domain.Usuario;


public interface UsuarioInterface  extends CrudRepository<Usuario,Long> {
	@Override
    List<Usuario> findAll();
	
	Usuario findByUsuario(String usuario);
	
	
}
