import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserComponent } from './i-user.component';

describe('IUserComponent', () => {
  let component: IUserComponent;
  let fixture: ComponentFixture<IUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
