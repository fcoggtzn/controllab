import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import {UsrUser} from '../UsrUser';
@Component({
  selector: 'app-i-user',
  templateUrl: './i-user.component.html',
  styleUrls: ['./i-user.component.scss']
})
export class IUserComponent implements OnInit {
  displayedColumns: string[] = ['user_usuario', 'user_password'];
  data: UsrUser[] = [];
  isLoadingResults = true;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getUsers()
      .subscribe(res => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

}
