import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {TipoUser} from 'src/app/i-user-tipo/TipoUser';


@Component({
  selector: 'app-i-user-edit',
  templateUrl: './i-user-edit.component.html',
  styleUrls: ['./i-user-edit.component.scss']
})
export class IUserEditComponent implements OnInit {
  tus: TipoUser[] = [
    {id: 1, nombre: 'Super Administrador', descripcion:'',nivel:''},
    {id: 1, nombre: 'Administrador', descripcion:'',nivel:''},
    {id: 1, nombre: 'Usuario', descripcion:'',nivel:''},
    {id: 1, nombre: 'invitado', descripcion:'',nivel:''}
  ];
  userForm: FormGroup;
  id: number = 0;
  user_user: string = '';
  user_password: string = '';
  isLoadingResults = false;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getUser(this.route.snapshot.params['id']);
    this.userForm = this.formBuilder.group({
      'user_user' : [null, Validators.required],
      'user_password' : [null, Validators.required],
    });
  }

  getUser(id) {
    this.api.getUser(id).subscribe(data => {
      this.id = data.id;
      this.userForm.setValue({
        user_user: data.user,
        user_password: data.password,
      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.updateUser(this.id, form)
      .subscribe(res => {
          let id = res['id'];
          this.isLoadingResults = false;
          this.router.navigate(['/i-user-detail', id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  userDetails() {
    this.router.navigate(['/i-user-details', this.id]);
  }

}
