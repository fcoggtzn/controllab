import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserEditComponent } from './i-user-edit.component';

describe('IUserEditComponent', () => {
  let component: IUserEditComponent;
  let fixture: ComponentFixture<IUserEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
