import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {TipoUser} from 'src/app/i-user-tipo/TipoUser';

@Component({
  selector: 'app-i-user-add',
  templateUrl: './i-user-add.component.html',
  styleUrls: ['./i-user-add.component.scss']
})
export class IUserAddComponent implements OnInit {
  tus: TipoUser[] = [
    {id: 1, nombre: 'Super Administrador', descripcion:'',nivel:''},
    {id: 1, nombre: 'Administrador', descripcion:'',nivel:''},
    {id: 1, nombre: 'Usuario', descripcion:'',nivel:''},
    {id: 1, nombre: 'invitado', descripcion:'',nivel:''}
  ];
  userForm: FormGroup;
  user_user: string = '';
  user_password: string = '';
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      'user_user': [null, Validators.required],
      'user_password': [null, Validators.required],

    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addUser(form)
      .subscribe(res => {
        let id = res['id'];
        this.isLoadingResults = false;
        this.router.navigate(['/i-user-detail', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

}
