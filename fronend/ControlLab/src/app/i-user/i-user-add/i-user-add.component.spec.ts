import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserAddComponent } from './i-user-add.component';

describe('IUserAddComponent', () => {
  let component: IUserAddComponent;
  let fixture: ComponentFixture<IUserAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
