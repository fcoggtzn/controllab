import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserDetailComponent } from './i-user-detail.component';

describe('IUserDetailComponent', () => {
  let component: IUserDetailComponent;
  let fixture: ComponentFixture<IUserDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
