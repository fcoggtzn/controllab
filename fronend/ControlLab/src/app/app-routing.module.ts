import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PruebaComponent } from './prueba/prueba.component';
import { AuthGuard } from './_auth/auth.guard';

import { IUserComponent } from './i-user/i-user-home/i-user.component';
import { IUserAddComponent } from './i-user/i-user-add/i-user-add.component';
import { IUserDetailComponent } from './i-user/i-user-detail/i-user-detail.component';
import { IUserEditComponent } from './i-user/i-user-edit/i-user-edit.component';
import { IUserTipoComponent } from './i-user-tipo/i-user-tipo/i-user-tipo.component';
import { IUserTipoAddComponent } from './i-user-tipo/i-user-tipo-add/i-user-tipo-add.component';
import { IUserTipoEditComponent } from './i-user-tipo/i-user-tipo-edit/i-user-tipo-edit.component';
import { IUserTipoDetailComponent } from './i-user-tipo/i-user-tipo-detail/i-user-tipo-detail.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'prueba', component: PruebaComponent ,  canActivate: [AuthGuard] },

  {
    path: 'i-user',
    component: IUserComponent,
    data: { title: 'Lista de Usuarios' }
  },
  {
    path: 'i-user-detail/:id',
    component: IUserDetailComponent,
    data: { title: 'Product Details' }
  },
  {
    path: 'i-user-add',
    component: IUserAddComponent,
    data: { title: 'Adicionar Usuario' }
  },
  {
    path: 'i-user-edit/:id',
    component: IUserEditComponent,
    data: { title: 'Editar Usuario' }
  },
  {
    path: 'i-user-tipo',
    component: IUserTipoComponent,
    data: { title: 'Lista de Tipo de Usuarios' }
  },
  {
    path: 'i-user-tipo-add',
    component: IUserTipoAddComponent,
    data: { title: 'Add de Tipo de Usuarios' }
  },
  {
    path: 'i-user-tipo-detail',
    component: IUserTipoDetailComponent,
    data: { title: 'Detalle de Tipo de Usuarios' }
  },
  {
    path: 'i-user-tipo-edit',
    component: IUserTipoEditComponent,
    data: { title: 'Editar de Tipo de Usuarios' }
  },
  /*{
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
