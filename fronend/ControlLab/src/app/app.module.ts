import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatMenuModule } from '@angular/material/menu';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatInputModule} from "@angular/material/input";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule} from "@angular/material/sort";
import { MatTableModule} from "@angular/material/table";
import { MatIconModule} from "@angular/material/icon";
import { MatButtonModule} from "@angular/material/button";
import { MatCardModule} from "@angular/material/card";
import { MatFormFieldModule} from "@angular/material/form-field";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatSelectModule} from '@angular/material/select';


//para autenticar
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { AddUsuarioComponent } from './add-usuario/add-usuario.component';
import { LayoutMasterComponent } from './layout-master/layout-master.component';


//uso de jwt
import { JwtInterceptorService } from './_helpers/jwt-interceptor.service';
import { AuthService } from './_auth/auth.service';
import { AuthGuard } from './_auth/auth.guard';
import { PruebaComponent } from './prueba/prueba.component';

import { IUserComponent } from './i-user/i-user-home/i-user.component';
import { IUserAddComponent } from './i-user/i-user-add/i-user-add.component';
import { IUserDetailComponent } from './i-user/i-user-detail/i-user-detail.component';
import { IUserEditComponent } from './i-user/i-user-edit/i-user-edit.component';
import { IUserTipoAddComponent } from './i-user-tipo/i-user-tipo-add/i-user-tipo-add.component';
import { IUserTipoEditComponent } from './i-user-tipo/i-user-tipo-edit/i-user-tipo-edit.component';
import { IUserTipoDetailComponent } from './i-user-tipo/i-user-tipo-detail/i-user-tipo-detail.component';
import { IUserTipoComponent } from './i-user-tipo/i-user-tipo/i-user-tipo.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent,
    FooterComponent,
    UsuarioComponent,
    LayoutMasterComponent,
    AddUsuarioComponent,
    PruebaComponent,
    IUserComponent,
    IUserAddComponent,
    IUserDetailComponent,
    IUserEditComponent,
    IUserTipoAddComponent,
    IUserTipoEditComponent,
    IUserTipoDetailComponent,
    IUserTipoComponent
  ],
  imports: [
    BrowserModule,
    MatMenuModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true },
    AuthService,
    AuthGuard,

  ],

  bootstrap: [AppComponent]
})
export class AppModule {
  btnClick= function () {
    this.router.navigate(['Inicio']);
};
}
