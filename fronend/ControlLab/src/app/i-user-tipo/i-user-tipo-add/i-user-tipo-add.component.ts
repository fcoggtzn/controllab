import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-i-user-tipo-add',
  templateUrl: './i-user-tipo-add.component.html',
  styleUrls: ['./i-user-tipo-add.component.scss']
})
export class IUserTipoAddComponent implements OnInit {
  userTipoForm: FormGroup;
  tipoUserNombre: string = '';
  tipoUserDescripcion: string = '';
  tipoUserNivel: string = '';
  isLoadingResults = false;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.userTipoForm = this.formBuilder.group({
      'tipoUserNombre': [null, Validators.required],
      'tipoUserDescripcion': [null, Validators.required],
      'tipoUserNivel': [null, Validators.required],
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addTipoUser(form)
      .subscribe(res => {
        let id = res['id'];
        this.isLoadingResults = false;
        this.router.navigate(['/i-user-tipo-detail', id]);
      }, (err) => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

}
