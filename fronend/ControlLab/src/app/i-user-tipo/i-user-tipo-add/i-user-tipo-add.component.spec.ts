import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserTipoAddComponent } from './i-user-tipo-add.component';

describe('IUserTipoAddComponent', () => {
  let component: IUserTipoAddComponent;
  let fixture: ComponentFixture<IUserTipoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserTipoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserTipoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
