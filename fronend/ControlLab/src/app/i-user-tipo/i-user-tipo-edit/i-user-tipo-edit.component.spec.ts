import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserTipoEditComponent } from './i-user-tipo-edit.component';

describe('IUserTipoEditComponent', () => {
  let component: IUserTipoEditComponent;
  let fixture: ComponentFixture<IUserTipoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserTipoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserTipoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
