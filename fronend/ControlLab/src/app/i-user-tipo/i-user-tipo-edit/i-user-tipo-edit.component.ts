import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-i-user-tipo-edit',
  templateUrl: './i-user-tipo-edit.component.html',
  styleUrls: ['./i-user-tipo-edit.component.scss']
})
export class IUserTipoEditComponent implements OnInit {
  userTipoForm: FormGroup;
  id: number = 0;
  tipoUsuario: string = '';
  descripcion: string = '';
  nivel: string = '';
  isLoadingResults = false;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getTipoUser(this.route.snapshot.params['id']);
    this.userTipoForm = this.formBuilder.group({
      'tipoUsuario' : [null, Validators.required],
      'descripcion' : [null, Validators.required],
      'valor' : [null, Validators.required],
    });
  }

  getTipoUser(id) {
    this.api.getTipoUser(id).subscribe(data => {
      this.id = data.id;
      this.userTipoForm.setValue({
        tipoUsuario: data.nombre,
        descripcion: data.descripcion,
        nivel: data.nivel,
      });
    });
  }

  onFormSubmit(form:NgForm) {
    this.isLoadingResults = true;
    this.api.updateTipoUser(this.id, form)
      .subscribe(res => {
          let id = res['id'];
          this.isLoadingResults = false;
          this.router.navigate(['/i-user-tipo-detail', id]);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  tipoUserDetails() {
    this.router.navigate(['/i-user-tipo-details', this.id]);
  }

}
