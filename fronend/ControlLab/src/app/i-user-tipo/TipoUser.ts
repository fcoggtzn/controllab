export class TipoUser {
  id: number;
  nombre: string;
  descripcion: string;
  nivel: string;
}
