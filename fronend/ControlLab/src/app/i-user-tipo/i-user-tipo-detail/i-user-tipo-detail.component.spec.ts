import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserTipoDetailComponent } from './i-user-tipo-detail.component';

describe('IUserTipoDetailComponent', () => {
  let component: IUserTipoDetailComponent;
  let fixture: ComponentFixture<IUserTipoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserTipoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserTipoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
