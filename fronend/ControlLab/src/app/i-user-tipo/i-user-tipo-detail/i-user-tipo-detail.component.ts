import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import {TipoUser} from '../TipoUser';

@Component({
  selector: 'app-i-user-tipo-detail',
  templateUrl: './i-user-tipo-detail.component.html',
  styleUrls: ['./i-user-tipo-detail.component.scss']
})
export class IUserTipoDetailComponent implements OnInit {
  tipoUser: TipoUser = { id: null, nombre: '', descripcion: '', nivel: ''};
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.getTipoUserDetails(this.route.snapshot.params['id']);
  }

  getTipoUserDetails(id) {
    this.api.getTipoUser(id)
      .subscribe(data => {
        this.tipoUser = data;
        console.log(this.tipoUser);
        this.isLoadingResults = false;
      });
  }

  deleteTipoUser(id) {
    this.isLoadingResults = true;
    this.api.deleteTipoUser(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/i-user-tipo']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

}
