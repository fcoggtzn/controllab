import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IUserTipoComponent } from './i-user-tipo.component';

describe('IUserTipoComponent', () => {
  let component: IUserTipoComponent;
  let fixture: ComponentFixture<IUserTipoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IUserTipoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IUserTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
