import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import {TipoUser} from '../TipoUser';

@Component({
  selector: 'app-i-user-tipo',
  templateUrl: './i-user-tipo.component.html',
  styleUrls: ['./i-user-tipo.component.scss']
})
export class IUserTipoComponent implements OnInit {
  displayedColumns: string[] = ['tipo_usuario_nombre', 'tipo_usuario_descripcion', 'tipo_usuario_nivel'];
  data: TipoUser[] = [];
  isLoadingResults = true;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getTiposUsers()
      .subscribe(res => {
        this.data = res;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }

}
