import { Injectable } from '@angular/core';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { UsrUser } from '../i-user/UsrUser';
import { TipoUser } from '../i-user-tipo/TipoUser';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrlUsr = "/api/v1/usuarios";
const apiUrlTipoUsr = "/api/v1/tipoUsuarios";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getUsers(): Observable<UsrUser[]> {
    return this.http.get<UsrUser[]>(apiUrlUsr)
      .pipe(
        tap(heroes => console.log('fetched User')),
        catchError(this.handleError('getUser', []))
      );
  }

  getUser(id: number): Observable<UsrUser> {
    const url = `${apiUrlUsr}/${id}`;
    return this.http.get<UsrUser>(url).pipe(
      tap(_ => console.log(`fetched User id=${id}`)),
      catchError(this.handleError<UsrUser>(`getUser id=${id}`))
    );
  }

  addUser(user): Observable<UsrUser> {
    return this.http.post<UsrUser>(apiUrlUsr, user, httpOptions).pipe(
      tap((user: UsrUser) => console.log(`added User w/ id=${user.id}`)),
      catchError(this.handleError<UsrUser>('addUser'))
    );
  }

  updateUser(id, user): Observable<any> {
    const url = `${apiUrlUsr}/${id}`;
    return this.http.put(url, user, httpOptions).pipe(
      tap(_ => console.log(`updated user id=${id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser(id): Observable<UsrUser> {
    const url = `${apiUrlUsr}/${id}`;

    return this.http.delete<UsrUser>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted user id=${id}`)),
      catchError(this.handleError<UsrUser>('deleteUser'))
    );
  }

  getTiposUsers(): Observable<TipoUser[]> {
    return this.http.get<TipoUser[]>(apiUrlTipoUsr)
      .pipe(
        tap(heroes => console.log('fetched Tipo User')),
        catchError(this.handleError('getTipoUser', []))
      );
  }

  getTipoUser(id: number): Observable<TipoUser> {
    const url = `${apiUrlTipoUsr}/${id}`;
    return this.http.get<TipoUser>(url).pipe(
      tap(_ => console.log(`fetched tipo User id=${id}`)),
      catchError(this.handleError<TipoUser>(`getTipoUser id=${id}`))
    );
  }

  addTipoUser(tipoUser): Observable<TipoUser> {
    return this.http.post<TipoUser>(apiUrlTipoUsr, tipoUser, httpOptions).pipe(
      tap((tipoUser: TipoUser) => console.log(`added tipo User w/ id=${tipoUser.id}`)),
      catchError(this.handleError<TipoUser>('addTipoUser'))
    );
  }

  updateTipoUser(id, tipoUser): Observable<any> {
    const url = `${apiUrlTipoUsr}/${id}`;
    return this.http.put(url, tipoUser, httpOptions).pipe(
      tap(_ => console.log(`updated tipo user id=${id}`)),
      catchError(this.handleError<any>('updateTipoUser'))
    );
  }

  deleteTipoUser(id): Observable<TipoUser> {
    const url = `${apiUrlTipoUsr}/${id}`;
    return this.http.delete<TipoUser>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted tipo user id=${id}`)),
      catchError(this.handleError<TipoUser>('deleteTipoUser'))
    );
  }

}
