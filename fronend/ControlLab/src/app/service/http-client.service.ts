import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class Usuario {
  constructor(
    public idUsuario: number,
    public usuario: string,
    public clave: string,
    public tipoUsuario: any,
  ) { }
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {


  constructor(private httpClient: HttpClient) {
  }

  getUsuario() {
    return this.httpClient.get<Usuario[]>('http://localhost:8080/usuario');
  }

  public deleteUsuario(usuario) {
    return this.httpClient.delete<Usuario>("http://localhost:8080/usuario" + "/" + usuario.idUsuario);
  }

  public createUsuario(usuario) {
    return this.httpClient.post<Usuario>("http://localhost:8080/usuario", usuario);
  }
}
