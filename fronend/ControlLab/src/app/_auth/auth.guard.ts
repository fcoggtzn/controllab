import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean  {
      let token = localStorage.getItem('ACCESS_TOKEN');
      if (!token || this.isTokenExpired(token)) {
        // Store the attempted URL for redirecting later
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
      }
      return true;
    }


    isTokenExpired(token): boolean {
      if(!token) return true;
  
      const date = this.getTokenExpirationDate(token);
      if(date === undefined) return false;
      return !(date.valueOf() > new Date().valueOf());
    }
  
    getTokenExpirationDate(token: string): Date {
      const decoded = jwt_decode(token);
  
      if (decoded.exp === undefined) return null;
  
      const date = new Date(0); 
      date.setUTCSeconds(decoded.exp);
      return date;
    }
  }
