import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { tap, map, catchError, finalize } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { JwtResponse } from '../_models/JwtResponse';
import { of } from 'rxjs';

(window as any).global = window;

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  //headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'}); <-- Not in use

  redirectUrl: string;
  private uri = environment.api ;
  authSubject = new BehaviorSubject(false);

  constructor(private http: HttpClient) { }
 
  login(data): Observable<JwtResponse> {
    alert(data);
    return this.http.post<JwtResponse>(`${this.uri}/authenticate`, data)
    .pipe(tap((res: JwtResponse) => {
      if (res.token) {
        localStorage.setItem("ACCESS_TOKEN", res.token);
       /* localStorage.setItem("CURR_USER", JSON.stringify(res.user));
        localStorage.setItem("CURR_CUENTA", JSON.stringify(res.cuenta));*/
      }
      return res;
    }))
    .pipe(
      catchError(this.handleError<any>('Authenticating'))
    );
  }

  forgot_password(data): Observable<String> {
    return this.http.post<String>(`${this.uri}/forgot_password`, data);
  }

  reset_password(data): Observable<JwtResponse> {
    this.logout();
    return this.http.post<JwtResponse>(`${this.uri}/reset_password`, data)
    .pipe(tap((res: JwtResponse) => {
      if (res.token) {
        localStorage.setItem("ACCESS_TOKEN", res.token);
      /*  localStorage.setItem("CURR_USER", JSON.stringify(res.user));
        localStorage.setItem("CURR_CUENTA", JSON.stringify(res.cuenta));*/
      }
      return res;
    }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('CURR_USER');
    localStorage.removeItem('CURR_CUENTA');
  }
  
  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      //alert(error.error.error)

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
