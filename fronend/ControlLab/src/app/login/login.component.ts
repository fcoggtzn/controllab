import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading = false;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) { }


  ngOnInit() {
    // reset login status
    this.authService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';


  }

  login(data): void {
    this.loading = true;

    this.authService.login(data.value)
    .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
    },
    error => {
        this.loading = false;
        console.log(error)
    });
  }

  btnClick(){
    this.router.navigate(['/inicio']);
   }

}


